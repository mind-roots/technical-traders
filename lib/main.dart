import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:technicaltraders/home/Home.dart';
import 'package:technicaltraders/login/Login.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';

import 'notification/notifications.dart';

SharedPrefs appAuth = new SharedPrefs();
//void main()=> runApp(MyApp());
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Widget _defaultHome = new MyApp();
  bool _result = await appAuth.login();
  if (_result) {
    _defaultHome = new Home();
  }
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Technical Traders',
    home: _defaultHome,
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var prefs = SharedPrefs();
  var isAuthAvailable = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();
    /*_firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        //print('on message $message');
        // var jj = json.decode(message.toString());
        var body = message['notification']['body'];
        var title = message['notification']['title'];
        var body1 = body;
        if (title == null || title == "null") {
          title = "Alert!";
        }
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Center(
                child: Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              content: new Text(body),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(
                    "Ok",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Notifications()),
                    );
                  },
                ),
              ],
            );
          },
        );
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResumeMain");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Notifications()),
        );
        },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunchMain");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Notifications()),
        );
        },
    );*/
    // isLogged = getLogged();
//    _firebaseMessaging.configure();
    _firebaseMessaging.subscribeToTopic("technicaltraders");
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Technical Traders',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Source Sans Pro'),
      home: Login(),
    );
  }

  void firebaseCloudMessaging_Listeners() {
    setState(() {
      if (Platform.isIOS) iOS_Permission();

      _firebaseMessaging.getToken().then((token) {
        print("TTTTOOOKKEEENNNZZZZZZZZZZZZ : " + token);
      });
    });
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}
