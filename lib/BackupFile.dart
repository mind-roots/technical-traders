import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'details/itemDetails.dart';
import 'details/itemModel.dart';

class NewsLetter extends StatefulWidget {
  @override
  NewsLetterState createState() {
    return new NewsLetterState();
  }
}

class NewsLetterState extends State<NewsLetter> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  Color color;

  @override
  void initState() {
    super.initState();
    color = Color(0xff949494);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Color(0xff3C3950)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset('assets/logo.png', height: 50.0, fit: BoxFit.cover),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(32.0, 12.0, 4.0, 10.0),
            child: Text("Wealth Building Newsletter",
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xff3C3950),
                )),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0.0, 48.0, 4.0, 10.0),
            child: ListView(
              children: items.map((item) {
                return ListTile(
                  dense: true,
                  title: Text(item.title,
                      style: TextStyle(color: color, fontSize: 16.0)),
                  trailing: Icon(Icons.arrow_forward_ios, color: color),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ItemDetails("sad", "","",""),
                      ),
                    );
                  },
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
