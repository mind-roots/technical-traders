import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:technicaltraders/Constants.dart';
import 'package:technicaltraders/home/Home.dart';
import 'package:technicaltraders/login/LoginBloc.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:url_launcher/url_launcher.dart';
import '../connectivity.dart';

class Login extends StatefulWidget {
  @override
  _TestSignInViewState createState() => new _TestSignInViewState();
}

TextStyle style = TextStyle(fontFamily: 'Source Sans Pro', fontSize: 20.0);
final logger = Logger();
bool _load = false;
final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

class _TestSignInViewState extends State<Login> {
  TextEditingController userController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  Map _source = {ConnectivityResult.none: false};
  MyConnectivity _connectivity = MyConnectivity.instance;
  bool pass = false, user = false;
  Image myImage, myImage1, userImage, passImage;
  Connectivity connectivity = Connectivity();
  bool isOnline = false;
  var prefs = SharedPrefs();
  bool isButtonPressed = false;

  @override
  void initState() {
    super.initState();
    myImage = Image.asset("assets/images/login_username_selected.png");
    userImage = Image.asset("assets/images/login_username.png");
    myImage1 = Image.asset("assets/images/login_password_selected.png");
    passImage = Image.asset("assets/images/login_password.png");
  }

  @override
  void dispose() {
    _connectivity.disposeStream();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(myImage.image, context);
    precacheImage(userImage.image, context);
    precacheImage(myImage1.image, context);
    precacheImage(passImage.image, context);
  }

  void initialise() async {
    ConnectivityResult result = await connectivity.checkConnectivity();

    if (result != ConnectivityResult.none) {
      bool resultSecond = await DataConnectionChecker().hasConnection;
      if (resultSecond) {
        setState(() {
          _load = true;
        });
        hitLoginService(context);
      } else {
        Constants.showToast(context, "Please enable internet");
        isOnline = false;
        setState(() {
          isButtonPressed = false;
        });
      }
    } else {
      Constants.showToast(context, "Please enable internet");
      isOnline = false;
      setState(() {
        isButtonPressed = false;
      });
    }

//    _checkStatus(result);
//    connectivity.onConnectivityChanged.listen((result) {
//      _checkStatus(result);
//    });
  }

  void _checkStatus(ConnectivityResult result) async {
    isOnline = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        isOnline = true;
        setState(() {
          _load = true;
        });
        hitLoginService(context);
      } else {
        Constants.showToast(context, "Please enable internet");
        isOnline = false;
        setState(() {
          isButtonPressed = false;
        });
      }
    } on SocketException catch (_) {
      Constants.showToast(context, "Please enable internet");
      isOnline = false;
      setState(() {
        isButtonPressed = false;
      });
    }
  }

  void checkFields() {
    if (userController.text.isEmpty || passwordController.text.isEmpty) {
      Constants.showToast(context, "Please enter both username and password");
    } else {
      setState(() {
        isButtonPressed = true;
      });
      initialise();
    }
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      controller: userController,
      onTap: () {
        setState(() {
          user = true;
          pass = false;
        });
      },
      onEditingComplete: () {
        setState(() {
          user = false;
        });
        Constants().hideKeyBoard(context);
      },
      obscureText: false,
      style: style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 20.0),
        prefixIcon: Container(
          padding:
              EdgeInsets.only(left: 24.0, right: 8.0, bottom: 12.0, top: 12.0),
          child: Image.asset(user
              ? "assets/images/login_username_selected.png"
              : "assets/images/login_username.png"),
        ),
        hintText: "Username or Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
        labelStyle: TextStyle(color: Color(0xff1E73BE), fontSize: 20.0),
      ),
    );
    final passwordField = TextField(
      controller: passwordController,
      obscureText: true,
      onTap: () {
        setState(() {
          user = false;
          pass = true;
        });
      },
      onEditingComplete: () {
        setState(() {
          pass = false;
        });
        Constants().hideKeyBoard(context);
      },
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 20.0),
          prefixIcon: Container(
            padding: EdgeInsets.only(
                left: 24.0, right: 8.0, bottom: 12.0, top: 12.0),
            child: Image.asset(pass
                ? "assets/images/login_password_selected.png"
                : "assets/images/login_password.png"),
          ),
          hintText: "Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          labelStyle: TextStyle(
              color: Color(0xff1E73BE),
              fontSize: 20.0,
              fontFamily: 'Source Sans Pro')),
    );
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff3C3950),
      child: MaterialButton(
        minWidth: 200,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          isButtonPressed ? null : checkFields();
        },
        child: Text(
          isButtonPressed ? "Please Wait..." : "LOG IN",
          textAlign: TextAlign.center,
          style: style.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Source Sans Pro'),
        ),
      ),
    );
    Widget loadingIndicator = _load
        ? new Container(
            color: Colors.white,
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    return Scaffold(
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints:
              BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            color: Color(0xff3C3950),
            child: Container(
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(64.0),
                    bottomRight: Radius.circular(64.0)),
                shape: BoxShape.rectangle,
              ),
              child: Padding(
                padding: const EdgeInsets.all(28.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 30.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 80.0,
                              child: Image.asset(
                                "assets/images/logo.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                            SizedBox(
                              height: 40,
                              child: Text(
                                "Log in to your account",
                                style: TextStyle(
                                    height: 2,
                                    fontSize: 16,
                                    fontFamily: 'Source Sans Pro'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 30),
                            ),
                            SizedBox(height: 30.0),
                            emailField,
                            SizedBox(height: 30.0),
                            passwordField,
                            SizedBox(
                              height: 30.0,
                            ),
                            loginButton,
                            SizedBox(
                              height: 15.0,
                            )
                          ],
                        ),
                      ),
                    ),
//                    GestureDetector(
//                        onTap: _launchURL,
//                        child: Container(
//                          child: Column(
//                            children: <Widget>[
//                              Text(
//                                'Become a Technical Trader',
//                                style: TextStyle(
//                                    height: 2,
//                                    fontSize: 16,
//                                    fontFamily: 'Source Sans Pro'),
//                              ),
//                              Text(
//                                'Sign Up Today',
//                                style: TextStyle(
//                                    height: 2,
//                                    fontSize: 16,
//                                    fontFamily: 'Source Sans Pro',
//                                    color: Color(0xff1E73BE),
//                                    fontWeight: FontWeight.bold),
//                              )
//                            ],
//                          ),
//                        )),
                    new Align(
                      child: loadingIndicator,
                      alignment: FractionalOffset.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  hitLoginService(BuildContext context) {
    Constants().hideKeyBoard(context);
    var bloc = LoginBloc();
    if (Platform.isIOS) iOS_Permission();
    String s = "";
    _firebaseMessaging.getToken().then((token) {
      s = token;
      print("TTTTOOOKKEEENNNZZZZZZZZZZZZ : " + token);
      if (Platform.isIOS) {
        bloc.fetchLoginData(userController.text.trim(),
            passwordController.text.trim(), "2", token);
      } else if (Platform.isAndroid) {
        bloc.fetchLoginData(userController.text.trim(),
            passwordController.text.trim(), "1", token);
      }
    });

    bloc.loginData.listen((response) {
      if (mounted) {
        setState(() {
          _load = false;
          isButtonPressed = false;
        });
      }
      if (response.status == "true") {
        if (response.msg.isEmpty) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => Home()),
              (Route<dynamic> route) => false);
        } else {
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Center(
                  child: Text(
                    "Alert!!",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Source Sans Pro',
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                content: new Text(
                  response.msg,
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Source Sans Pro',
                    fontSize: 18.0,
                  ),
                ),
                actions: <Widget>[
                  new FlatButton(
                    child: new Text(
                      "Ok",
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => Home()),
                          (Route<dynamic> route) => false);
                    },
                  ),
                ],
              );
            },
          );
        }
      } else {
        setState(() {
          isButtonPressed = false;
        });
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Center(
                child: Text(
                  "Alert!!",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              content: new Text(
                response.msg,
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 18.0,
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(
                    "Ok",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    });
  }

  _launchURL() async {
    const url = 'https://www.thetechnicaltraders.com/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}
