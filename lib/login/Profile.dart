import 'package:technicaltraders/home/ResourceModel.dart';

class Profile {
  String id;
  String user_id;
  String login;
  String email;
  String created_at;
  List<ResourceModel> resource;

  Profile(collection) {
    id = collection['id'];
    user_id = collection['user_id'];
    login = collection['login'];
    email = collection['email'];
    created_at = collection['created_at'];
    if (collection['resources'].length > 0) {
      List<ResourceModel> temp = [];
      for (int i = 0; i < collection['resources'].length; i++) {
        ResourceModel result = ResourceModel(collection['resources'][i]);
        temp.add(result);
      }
      resource = temp;
    }
  }
}
