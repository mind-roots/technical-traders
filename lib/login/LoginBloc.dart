import 'package:rxdart/rxdart.dart';
import 'package:technicaltraders/login/LoginModel.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:technicaltraders/resources/repository.dart';

class LoginBloc {
  final _repository = Repository();
  final _homeFetcher = PublishSubject<ModelLogin>();
  final _homeFetcher1 = PublishSubject<ModelLogin>();
  var prefs = SharedPrefs();

  Observable<ModelLogin> get loginData => _homeFetcher.stream;
  Observable<ModelLogin> get loginData1 => _homeFetcher1.stream;

  fetchLoginData(userName, password, deviceType, deviceId) async {
    ModelLogin itemModel = await _repository.fetchLoginData(
        userName, password, deviceType, deviceId);

    if (itemModel.status == "true") {
      prefs.setAuthCode(itemModel.auth_code);
      prefs.setUserPass(userName, password);
    }
    _homeFetcher.sink.add(itemModel);
  }

  fetchHomeLoginData(userName, password, deviceType, deviceId) async {
    var auth_code = await prefs.getAuthCode();
    ModelLogin itemModel = await _repository.fetchLoginHomeData(
        auth_code, userName, password, deviceType, deviceId);

    if (itemModel.status == "true") {
      prefs.setAuthCode(itemModel.auth_code);
      prefs.setUserPass(userName, password);
    }
    _homeFetcher1.sink.add(itemModel);
  }

  dispose() {
    _homeFetcher.close();
    _homeFetcher1.close();
  }
}
