import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:technicaltraders/home/ResourceModel.dart';
import 'package:technicaltraders/login/LoginModel.dart';
import 'package:technicaltraders/login/Profile.dart';

class ModelLogin {
  String status;
  String msg;
  String auth_code;
  Profile profile;


  ModelLogin.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];

    if (status == "true") {
      msg = parsedJson['msg'];
      auth_code = parsedJson['auth_code'];
      profile = Profile(parsedJson['profile']);

    } else if (status == "false") {
      msg = parsedJson['msg'];
    }
  }
}
