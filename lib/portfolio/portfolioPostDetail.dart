import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:technicaltraders/login/Login.dart';
import 'package:technicaltraders/portfolio/portfolioPostBloc.dart';
import 'package:technicaltraders/portfolio/portfolioPostModel.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../notification/notifications.dart';
import '../progressHud.dart';

// ignore: must_be_immutable
class PortFolioDetails extends StatefulWidget {
  var link;
  var portfolio;
  var education;
  var index = -1;

  PortFolioDetails(link, String portfolio, String education, int index) {
    this.link = link;
    this.portfolio = portfolio;
    this.education = education;
    this.index = index;
  }

  //ItemDetails({@required this.item});
  @override
  State<StatefulWidget> createState() => PortFolioDetailsState();
}

//final Item item;
class PortFolioDetailsState extends State<PortFolioDetails> {
  var bloc = PortfolioPostBloc();
  PortfolioModel itemsData;
  bool _load = false;
  InAppWebViewController _controller;
  String url = "";
  var prefs = SharedPrefs();

  @override
  void initState() {
    super.initState();
    print("PORTFOLIO PPPPOSST DETAILS");
    if (widget.portfolio.toString().isEmpty) {
      bloc.getPortfolio(widget.link, widget.index, widget.education);
      setState(() {
        _load = true;
      });
    } else {
      return;
    }

    bloc.itemData.listen((response) {
      if (response.status == "true") {
        setState(() {
          itemsData = response;
          // _controller.loadUrl(Uri.dataFromString(
          //         '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head>' +
          //             itemsData.resource.content
          //                 .replaceAll("width=", 'width="100%"'),
          //         mimeType: 'text/html',
          //         encoding: Encoding.getByName('utf-8'),
          //         base64: true)
          //     .toString());
          _load = false;
        });
      } else {
        if (mounted) {
          setState(() {
            _load = false;
          });
          if (response.msg.isNotEmpty) {
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                // return object of type Dialog
                return WillPopScope(
                  onWillPop: () {},
                  child: new AlertDialog(
                    title: new Center(
                      child: Text(
                        "Alert!!",
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Source Sans Pro',
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    content: new Text(
                      response.msg,
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Source Sans Pro',
                        fontSize: 18.0,
                      ),
                    ),
                    actions: <Widget>[
                      // usually buttons at the bottom of the dialog
                      new FlatButton(
                        child: new Text(
                          "Ok",
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                          prefs.clearPrefs();
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (context) => Login()),
                              (Route<dynamic> route) => false);
                        },
                      ),
                    ],
                  ),
                );
              },
            );
          }
        } else {
          return;
        }
      }
    });
  }

  void pageFinishedLoading(String url) {
    setState(() {
      _load = false;
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios,
              color: Color(0xff3C3950), size: 20.0),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        title: Image.asset('assets/images/logo.png',
            height: 50.0, fit: BoxFit.cover),
        actions: <Widget>[
          IconButton(
            icon: Image.asset('assets/images/notification.png'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Notifications()),
              );
            },
          ),
        ],
      ),
      body: ProgressHUD(
        child: Padding(
          padding: EdgeInsets.all(0.0),
          child: Stack(
            children: <Widget>[
              Container(
                color: Color(0xff3C3950),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 12.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(64.0),
                      bottomRight: Radius.circular(64.0)),
                  shape: BoxShape.rectangle,
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 36.0),
                  child: _load
                      ? Container()
                      : InAppWebView(
                          initialUrl: Uri.dataFromString(
                                  '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head>' +
                                      itemsData.resource.content
                                          .replaceAll("width=", 'width="100%"'),
                                  mimeType: 'text/html',
                                  encoding: Encoding.getByName('utf-8'),
                                  base64: true)
                              .toString(),
                          initialHeaders: {},
                          initialOptions: InAppWebViewGroupOptions(
                              crossPlatform: InAppWebViewOptions(
                            debuggingEnabled: true,
                          )),
                          onWebViewCreated:
                              (InAppWebViewController controller) {
                            _controller = controller;
                          },
                          onLoadStart:
                              (InAppWebViewController controller, String url) {
                            setState(() {
                              this.url = url;
                            });
                          },
                          onLoadStop: (InAppWebViewController controller,
                              String url) async {
                            setState(() {
                              this.url = url;
                            });
                          },
                          onProgressChanged: (InAppWebViewController controller,
                              int progress) {
                            setState(() {
                              // this.progress = progress / 100;
                            });
                          },
                        ),
                  // child: WebView(
                  //   gestureNavigationEnabled: true,
                  //   javascriptMode: JavascriptMode.unrestricted,
                  //   onWebViewCreated: (WebViewController webViewController) {
                  //     _controller = webViewController;
                  //     widget.portfolio.toString().isNotEmpty
                  //         ? _controller.loadUrl(widget.portfolio)
                  //         : Container();
                  //     _controller.canGoBack();
                  //     _controller.canGoForward();
                  //   },
                  //   navigationDelegate: (NavigationRequest request) {
                  //     if (request.url.startsWith(
                  //             'http://www.thetechnicaltraders.com') ||
                  //         request.url.startsWith(
                  //             'https://www.thetechnicaltraders.com') ||
                  //         request.url.contains("technical")) {
                  //       print('blocking navigation to $request}');
                  //       _launchURL(request.url);
                  //       return NavigationDecision.prevent;
                  //     }
                  //     setState(() {
                  //       _load = true;
                  //     });
                  //     print('allowing navigation to $request');
                  //     return NavigationDecision.navigate;
                  //   },
                  //   onPageFinished: pageFinishedLoading,
                  // ),
                ),
              )
            ],
          ),
        ),
        inAsyncCall: _load,
        opacity: 0.0,
      ),
    );
  }
}
