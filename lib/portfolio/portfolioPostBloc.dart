import 'package:rxdart/rxdart.dart';
import 'package:technicaltraders/details/itemModel.dart';
import 'package:technicaltraders/home/homeModel.dart';
import 'package:technicaltraders/latestPost/latestPostDetail.dart';
import 'package:technicaltraders/latestPost/latestPostModel.dart';
import 'package:technicaltraders/portfolio/portfolioPostModel.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:technicaltraders/resources/repository.dart';

class PortfolioPostBloc {
  final _repository = Repository();
  final _postFetcher = PublishSubject<PortfolioModel>();
  var prefs = SharedPrefs();
  Observable<PortfolioModel> get itemData => _postFetcher.stream;

  getPortfolio(resources_link, index, education) async {
    var auth_code = await prefs.getAuthCode();
    PortfolioModel itemModel = await _repository.getPortfolio(
        auth_code, resources_link, index, education);

    _postFetcher.sink.add(itemModel);
  }

  dispose() {
    _postFetcher.close();
  }
}
