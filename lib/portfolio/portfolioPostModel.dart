import 'package:technicaltraders/latestPost/latestPostDetailsModel.dart';

class PortfolioModel {
  String status = "";
  String msg = "";
  LatestPostDetailsModel resource;

  PortfolioModel.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];

    if (status == "true") {
      msg = parsedJson['msg'];
      LatestPostDetailsModel result =
          LatestPostDetailsModel(parsedJson['portfolio']);
      resource = result;
    }else if(status == "false"){
      msg = parsedJson['msg'];
    }
  }
}
