import 'dart:convert';

import 'package:meta/meta.dart';

import 'PostModel.dart';

class ItemModel {
  String status = "";
  String msg = "";
  List<PostModel> resource;
  String count;
  ItemModel.fromJson(List<dynamic> parsedJson, a) {
//    status = parsedJson['status'];

    count = a;
    List<PostModel> temp = [];
    for (int i = 0; i < parsedJson.length; i++) {
      PostModel result = PostModel(parsedJson[i]);
      temp.add(result);
    }
    if (temp.length > 0) {
      status = "true";
    } else {
      status = "false";
    }
    resource = temp;
  }
}

class Item {
  Item({
    @required this.title,
    @required this.subtitle,
  });

  final String title;
  final String subtitle;
}

final List<Item> items = <Item>[
  Item(
    title: 'Market Updates & Trade Signals',
    subtitle: 'This is the first item.',
  ),
  Item(
    title: 'Portfolio',
    subtitle: 'This is the second item.',
  ),
  Item(
    title: 'Education',
    subtitle: 'This is the third item.',
  ),
  Item(
    title: 'BAN Hotlink',
    subtitle: 'This is the fourth item.',
  ),
//  Item(
//    title: 'My Account',
//    subtitle: 'This is the third item.',
//  ),
];
