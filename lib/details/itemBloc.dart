import 'package:rxdart/rxdart.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:technicaltraders/resources/repository.dart';

import 'itemModel.dart';

class ItemBloc {
  final _repository = Repository();
  final _homeFetcher = PublishSubject<ItemModel>();
  var prefs=SharedPrefs();
  Observable<ItemModel> get itemData => _homeFetcher.stream;

  fetchItemData(resources_link,int perPage, String offset) async {
    var auth_code= await prefs.getAuthCode();
    ItemModel itemModel = await _repository.fetchItemData(auth_code,resources_link,perPage,offset);

    _homeFetcher.sink.add(itemModel);
  }

  dispose() {
    _homeFetcher.close();
  }
}