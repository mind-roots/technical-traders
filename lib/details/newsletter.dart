import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:technicaltraders/portfolio/portfolioPostDetail.dart';

import '../Constants.dart';
import 'itemDetails.dart';
import 'itemModel.dart';

class NewsLetter extends StatefulWidget {
  var link = "";
  var title = "";
  var image = "";
  var portfolio = "";
  var color = "";
  var education = "";

  NewsLetter(String link, String title, String image, String portfolio,
      String color, String education) {
    this.link = link;
    this.title = title;
    this.image = image;
    this.portfolio = portfolio;
    this.color = color;
    this.education = education;
  }

  @override
  NewsLetterState createState() {
    return new NewsLetterState();
  }
}

class NewsLetterState extends State<NewsLetter> {
  TextStyle style = TextStyle(fontFamily: 'Source Sans Pro', fontSize: 20.0);
  Color color;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    color = Color(0xff949494);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios,
              color: Color(0xff3C3950), size: 20.0),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset('assets/images/logo.png',
            height: 50.0, fit: BoxFit.cover),
        centerTitle: true,
      ),
      body: Stack(children: <Widget>[
        Container(
          margin: EdgeInsets.all(0.0),
          color: Color(0xff3C3950),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 12.0),
          padding: EdgeInsets.only(bottom: 20.0, left: 16.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(64.0),
                bottomRight: Radius.circular(64.0)),
            shape: BoxShape.rectangle,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Source Sans Pro',
                  color: HexColor.fromHex(widget.color),
                ),
              ),
              Flexible(
                child: ListView.builder(
                    itemCount: items.length,
                    shrinkWrap: true,
                    primary: false,
                    // scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext context, int index) {
//              children: items.map((item) {
                      return (widget.title == "BAN Trader Pro" && index == 3) ||
                              index < 3
                          ? Container(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      new Flexible(
                                        child: Container(
                                          padding: EdgeInsets.fromLTRB(
                                              16.0, 8.0, 0.0, 8.0),
                                          color: Color(0xff949494),
                                          width: 6.0,
                                          height: 70.0,
                                          margin: EdgeInsets.fromLTRB(
                                              2.0, 8.0, 0.0, 8.0),
                                        ),
                                        flex: 1,
                                      ),
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 6.0, 0.0, 16.0),
                                          decoration: BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  width: 0.3,
                                                  color: Color(0xff949494)),
                                            ),
                                            color: Colors.white,
                                          ),
                                          child: ListTile(
                                            contentPadding:
                                                EdgeInsets.only(left: 16.0),
                                            dense: true,
                                            title: Text(items[index].title,
                                                style: TextStyle(
                                                    color: color,
                                                    fontSize: 18.0,
                                                    fontFamily:
                                                        'Source Sans Pro')),
                                            trailing: Icon(
                                              Icons.arrow_forward_ios,
                                              color: color,
                                              size: 20.0,
                                            ),
                                            onTap: () {
                                              if (index == 0) {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (_) => ItemDetails(
                                                        widget.link,
                                                        widget.title,
                                                        widget.image,
                                                        widget.color),
                                                  ),
                                                );
                                              } else {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (_) =>
                                                        PortFolioDetails(
                                                            widget.link,
                                                            widget.portfolio,
                                                            widget.education,
                                                            index),
                                                  ),
                                                );
                                              }
                                            },
                                          ),
                                        ),
                                        flex: 8,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            )
                          : Container();
                    }),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
