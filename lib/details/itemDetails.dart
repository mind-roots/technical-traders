import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:intl/intl.dart';
import 'package:technicaltraders/details/itemBloc.dart';
import 'package:technicaltraders/latestPost/latestPostDetail.dart';

import '../Constants.dart';
import '../notification/notifications.dart';
import 'itemModel.dart';

// ignore: must_be_immutable
class ItemDetails extends StatefulWidget {
  var link;
  var title;
  var image;
  var color = "";

  ItemDetails(String link, String title, String image, String color) {
    this.link = link;
    this.title = title;
    this.image = image;
    this.color = color;
  }

  @override
  State<StatefulWidget> createState() => _ItemDetailsState();
}

class _ItemDetailsState extends State<ItemDetails> {
  //ItemDetails({@required this.item});
  var bloc = ItemBloc();
  ItemModel itemsData;
  bool _load = false;
  bool _loadPagination = false;
  var unescape = new HtmlUnescape();
  ScrollController _scrollController = new ScrollController();
  int page = 0;
  int offset = 10;
  int count = 0;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  DateFormat dateFormat = DateFormat("yyyy-MMM-dd HH:mm:ss");
  DateFormat dateFormatParse = DateFormat("yyyy-MM-dd HH:mm:ss");

  //final Item item;
  @override
  void initState() {
    super.initState();
    bloc.fetchItemData(widget.link, page, offset.toString());
    setState(() {
      _load = true;
    });
    bloc.itemData.listen((response) {
      count = int.parse(response.count);
      if (response.status == "true") {
        setState(() {
          if (itemsData != null) {
            itemsData.resource.addAll(response.resource);
          } else {
            itemsData = response;
          }
          _load = false;
          _loadPagination = false;
        });
      } else {
        if (mounted) {
          setState(() {
            _load = false;
            _loadPagination = false;
          });
        } else {
          return;
        }
      }
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (itemsData.resource.length < count &&
            itemsData.resource.length != 0) {
          setState(() {
            _loadPagination = true;
          });
          page += 10;
          bloc.fetchItemData(widget.link, page, offset.toString());
        } else {
          return;
        }
      }
    });
    //firebase message
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        //print('on message $message');
        // var jj = json.decode(message.toString());
        var body = message['notification']['body'];
        var title = message['notification']['title'];
        var body1 = body;
        if (title == null || title == "null") {
          title = "Alert!";
        }
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Center(
                child: Text(
                  title,
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              content: new Text(
                body,
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Source Sans Pro',
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(
                    "Ok",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Source Sans Pro'),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                    page = 0;
                    setState(() {
                      _load = true;
                    });
                    if (itemsData != null) {
                      itemsData.resource.clear();
                    }
                    bloc.fetchItemData(widget.link, page, offset.toString());
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    itemsData.resource.clear();
    _loadPagination = false;
    _load = false;
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            color: Colors.white,
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    Widget loadingIndicatorPagination = _loadPagination
        ? new Container(
            color: Colors.transparent,
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios,
              color: Color(0xff3C3950), size: 20.0),
          onPressed: () =>
              {_loadPagination = false, Navigator.of(context).pop()},
        ),
        backgroundColor: Colors.white,
        title: Image.asset('assets/images/logo.png',
            height: 50.0, fit: BoxFit.cover),
        actions: <Widget>[
          IconButton(
            icon: Image.asset('assets/images/notification.png'),
            onPressed: () {
              setState(() {
                _loadPagination = false;
              });

              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Notifications()),
              ).then((onValue) => {
                    page = 0,
                    if (itemsData != null) {itemsData.resource.clear()},
                    bloc.fetchItemData(widget.link, page, offset.toString()),
                    setState(() {
                      _load = true;
                      _loadPagination = false;
                    })
                  });
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(0.0),
            color: Color(0xff3C3950),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(64.0),
                  bottomRight: Radius.circular(64.0)),
              shape: BoxShape.rectangle,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Container(
//                        margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 10.0),
                        padding: EdgeInsets.all(32.0),
                        decoration: BoxDecoration(
                          color: HexColor.fromHex(widget.color),
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.all(Radius.circular(24.0)),
                        ),

                        // column of three rows
                        child: Column(
                          // this makes the column height hug its content
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 16.0,
                                    horizontal: 0.0,
                                  ),
                                ),
                                Text(
                                  widget.title,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Source Sans Pro',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 8.0),
                  child: Text(
                    "Latest Posts",
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Source Sans Pro',
                      color: Color(0xff3C3950),
                    ),
                  ),
                ),
                Flexible(
                  child: itemsData != null
                      ? Container(
                          margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 30.0),
                          child: ListView.builder(
                              itemCount: itemsData.resource.length,
                              controller: _scrollController,
                              itemBuilder: (BuildContext context, int index) {
                                return ListTile(
                                  contentPadding: EdgeInsets.all(0.0),
                                  dense: true,
//                                  leading: Image.network(
//                                    itemsData.resource[index].image,
//                                    height: 32.0,
//                                    width: 32.0,
//                                  ),
                                  title: Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        (widget.title == "BAN Trader Pro" &&
                                                    index == 3) ||
                                                index < 3
                                            ? Text(
                                                unescape.convert(itemsData
                                                    .resource[index].title),
                                                textAlign: TextAlign.justify,
                                                style: TextStyle(
                                                    color: Color(0xff3C3950),
                                                    fontSize: 18.0,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        'Source Sans Pro'))
                                            : Container(),
                                        Text(
                                            itemsData.resource[index].date.contains("T")
                                                ? dateFormat.format(
                                                    dateFormatParse.parse(itemsData
                                                        .resource[index].date
                                                        .replaceAll("T", " ")))
                                                : unescape.convert(dateFormat.format(
                                                    dateFormatParse.parse(
                                                        itemsData
                                                            .resource[index]
                                                            .date))),
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: Color(0xff949494),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0,
                                                fontFamily: 'Source Sans Pro')),
                                      ],
                                    ),
                                  ),
                                  subtitle: Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.only(top: 4),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                            unescape.convert(itemsData
                                                .resource[index].description
                                                .replaceAll("<p>", "")
                                                .replaceAll("</p>", "")
                                                .replaceAll("<[^>]*>", "")),
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                                color: Color(0xff949494),
                                                fontFamily: 'Source Sans Pro',
                                                fontSize: 18.0)),
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _loadPagination = false;
                                    });
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) => LatestPostDetail(
                                            itemsData.resource[index].id,
                                            widget.link),
                                      ),
                                    ).then((onValue) => {
                                          page = 0,
                                          if (itemsData != null)
                                            {itemsData.resource.clear()},
//                                          itemsData.resource.clear(),
                                          bloc.fetchItemData(widget.link, page,
                                              offset.toString()),
                                          setState(() {
                                            _load = true;
                                            _loadPagination = false;
                                          })
                                        });
                                  },
                                );
                              }),
                        )
                      : Container(),
                )
              ],
            ),
          ),
          new Align(
            child: loadingIndicator,
            alignment: FractionalOffset.center,
          ),
          new Align(
            child: loadingIndicatorPagination,
            alignment: FractionalOffset.bottomCenter,
          )
        ],
      ),
    );
  }
}
