import 'PostTitle.dart';

class PostModel{

  int id;
  String date;
  String description;
  String title;
  Map<String,dynamic> titleMainField;
  Map<String,dynamic> descriptionMainField;


  String image='assets/statistics.png';
  //String subtext='Big Trends, Big Profit Investing';

  PostModel(collection){
    id=collection["id"];
    date=collection["date"];
    description=collection["description"];
    //title=collection["title"];
    titleMainField=collection["title"];
    descriptionMainField=collection["excerpt"];
    title = titleMainField["rendered"];
    description = descriptionMainField["rendered"];
//    image=collection["image"];
    //image=collection["image"];

  }
}