import 'package:rxdart/rxdart.dart';
import 'package:technicaltraders/latestPost/latestPostModel.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:technicaltraders/resources/repository.dart';

class LatestPostBloc {
  final _repository = Repository();
  final _postFetcher = PublishSubject<LatestPostModel>();
  var prefs = SharedPrefs();
  Observable<LatestPostModel> get itemData => _postFetcher.stream;

  getPostdetail(resources_link,post_id) async {
    var auth_code = await prefs.getAuthCode();
    LatestPostModel itemModel =
        await _repository.getPostdetail(auth_code, resources_link,post_id);

    _postFetcher.sink.add(itemModel);
  }

  dispose() {
    _postFetcher.close();
  }
}
