import 'dart:convert';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:technicaltraders/latestPost/latestPostBloc.dart';
import 'package:technicaltraders/latestPost/latestPostModel.dart';
import 'package:technicaltraders/login/Login.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../notification/notifications.dart';
import '../progressHud.dart';

// ignore: must_be_immutable
class LatestPostDetail extends StatefulWidget {
  var id;
  var link;

  LatestPostDetail(int id, link) {
    this.link = link;
    this.id = id;
  }

  //ItemDetails({@required this.item});
  @override
  State<StatefulWidget> createState() => _LatestPostDetailState();
}

//final Item item;
class _LatestPostDetailState extends State<LatestPostDetail> {
  var bloc = LatestPostBloc();
  LatestPostModel itemsData;
  bool _load = false, showDialog = false;
  //WebViewController _controller;
  String message = "This post could not be found";
  static var verticalGestures = Factory<VerticalDragGestureRecognizer>(
      () => VerticalDragGestureRecognizer());
  var gestureSet = Set.from([verticalGestures]);
  var prefs = SharedPrefs();
  InAppWebViewController _controller;
  String url = "";
  @override
  void initState() {
    super.initState();
    print("LATEST PPPPOSST DETAILS");
    bloc.getPostdetail(widget.link, widget.id);
    setState(() {
      _load = true;
    });
    bloc.itemData.listen((response) {
      if (response.status == "true") {
        setState(() {
          itemsData = response;
          // _controller.loadUrl(Uri.dataFromString(
          //         '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head>' +
          //             itemsData.resource.content
          //                 .replaceAll("width=", 'width="100%"'),
          //         mimeType: 'text/html',
          //         encoding: Encoding.getByName('utf-8'),
          //         base64: true)
          //     .toString());
          _load = false;
        });
      } else if (response.status == "false") {
        message = response.msg;
        if (mounted) {
          setState(() {
            _load = false;
            showDialog = true;
          });
        } else {
          return;
        }
      } else {
        if (mounted) {
          setState(() {
            _load = false;
          });
        } else {
          return;
        }
      }
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void pageFinishedLoading(String url) {
    setState(() {
      _load = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0.0,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios,
                color: Color(0xff3C3950), size: 20.0),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Colors.white,
          title: Image.asset('assets/images/logo.png',
              height: 50.0, fit: BoxFit.cover),
          actions: <Widget>[
            IconButton(
              icon: Image.asset('assets/images/notification.png'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Notifications()),
                );
              },
            ),
          ],
        ),
        body: !showDialog
            ? ProgressHUD(
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        color: Color(0xff3C3950),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 12.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(64.0),
                              bottomRight: Radius.circular(64.0)),
                          shape: BoxShape.rectangle,
                        ),
                        child: Padding(
                          padding:
                              const EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 36.0),
                          child: _load?Container():InAppWebView(
                            initialUrl: Uri.dataFromString(
                                '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><link rel="stylesheet"href="https://fonts.googleapis.com/css?family=Source Sans Pro"><style>p{color:grey;font-family:Source Sans Pro}</style></head>' +
                                    itemsData.resource.content
                                        .replaceAll("width=", 'width="100%"'),
                                mimeType: 'text/html',
                                encoding: Encoding.getByName('utf-8'),
                                base64: true)
                                .toString(),
                            initialHeaders: {},
                            initialOptions: InAppWebViewGroupOptions(
                                crossPlatform: InAppWebViewOptions(
                                  debuggingEnabled: true,
                                )
                            ),
                            onWebViewCreated: (InAppWebViewController controller) {
                              _controller = controller;
                            },
                            onLoadStart: (InAppWebViewController controller, String url) {
                              setState(() {
                                this.url = url;
                              });
                            },
                            onLoadStop: (InAppWebViewController controller, String url) async {
                              setState(() {
                                this.url = url;
                              });
                            },
                            onProgressChanged: (InAppWebViewController controller, int progress) {
                              setState(() {
                                // this.progress = progress / 100;
                              });
                            },
                          ),
                          // child: WebView(
                          //   javascriptMode: JavascriptMode.unrestricted,
                          //   onWebViewCreated:
                          //       (WebViewController webViewController) {
                          //     _controller = webViewController;
                          //   },
                          //   navigationDelegate: (NavigationRequest request) {
                          //     if (request.url.startsWith(
                          //             'http://www.thetechnicaltraders.com') ||
                          //         request.url.startsWith(
                          //             'https://www.thetechnicaltraders.com') ||
                          //         request.url.contains("technical")) {
                          //       print('blocking navigation to $request}');
                          //       _launchURL(request.url);
                          //       return NavigationDecision.prevent;
                          //     }
                          //     setState(() {
                          //       _load = true;
                          //     });
                          //     print('allowing navigation to $request');
                          //     return NavigationDecision.navigate;
                          //   },
                          //   gestureNavigationEnabled: true,
                          //   onPageFinished: pageFinishedLoading,
                          //   debuggingEnabled: true,
                          //   initialMediaPlaybackPolicy:
                          //       AutoMediaPlaybackPolicy.always_allow,
                          // ),
                        ),
                      )
                    ],
                  ),
                ),
                inAsyncCall: _load,
                opacity: 0.0,
              )
            : CustomDialog(message));
  }
}

class CustomDialog extends StatelessWidget {
  final disputeNoteController = TextEditingController();
  String title;
  var prefs = SharedPrefs();

  CustomDialog(String s) {
    this.title = s;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 16,
            bottom: 16,
            left: 16,
            right: 16,
          ),
          margin: EdgeInsets.only(top: 16),
          decoration: new BoxDecoration(
            color: Colors.white70,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                  alignment: Alignment.topLeft,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
//                        child: Text(
//                          "Alert",
//                          style: style.copyWith(
//                              color: Colors.black,
//                              fontSize: 22,
//                              fontFamily: 'Source Sans Pro'),
//                          textAlign: TextAlign.left,
//                        ),
                      ),
                      SizedBox(height: 8.0),
                      Container(
                          width: double.infinity,
                          child: Text(title,
                              style: style.copyWith(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: 'Source Sans Pro'))),
                      SizedBox(height: 8.0),
                    ],
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (title.contains(
                          "You have been logged out as you logged into another phone.")) {
                        Navigator.of(context).pop(true);
                        prefs.clearPrefs();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (context) => Login()),
                            (Route<dynamic> route) => false);
                      } else {
                        Navigator.pop(context, true);
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
                      height: 50,
                      width: 55,
                      child: Card(
                        color: Color(0xff3C3950),
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Stack(
                          children: <Widget>[
                            Center(
                                child: Text("OK",
                                    style: style.copyWith(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontFamily: 'Source Sans Pro'))),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
