import 'package:technicaltraders/latestPost/latestPostDetailsModel.dart';

class LatestPostModel {
  String status = "";
  String msg = "";
  LatestPostDetailsModel resource;

  LatestPostModel.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];

    if (status == "true") {
      msg = parsedJson['msg'];
      LatestPostDetailsModel result =
          LatestPostDetailsModel(parsedJson['posts']);
      resource = result;
    } else if (status == "false") {
      msg = parsedJson['msg'];
    }
  }
}
