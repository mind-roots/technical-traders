class LatestPostDetailsModel {
  int id;
  String date;
  String title;
  String link;
  String content;

  LatestPostDetailsModel(collection) {
    id = collection["id"];
    date = collection["date"];
//    title = collection["title"];
    link = collection["link"];
    content = collection["content"];
  }
}
