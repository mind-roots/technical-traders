import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';

import 'login/Login.dart';

class Constants {
  //Function to hide KeyBoard--------------------
  //---------------------------------------------
  hideKeyBoard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static void showToast(BuildContext context, String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.white,
        fontSize: 18.0);
  }

  static showDialog(BuildContext context, String msg) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context, msg),
    );
  }

  static dialogContent(BuildContext context, String title) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 16,
            bottom: 16,
            left: 16,
            right: 16,
          ),
          margin: EdgeInsets.only(top: 16),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                  alignment: Alignment.topLeft,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        child: Text(
                          "Alert",
                          style: style.copyWith(
                              color: Colors.black,
                              fontSize: 22,
                              fontFamily: 'Source Sans Pro'),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Container(
                          width: double.infinity,
                          child: Text(title,
                              style: style.copyWith(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: 'Source Sans Pro'))),
                      SizedBox(height: 8.0),
                    ],
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context, false);
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => EndTimePage()),
//                  );
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
                        height: 50,
                        width: 55,
                        child: Card(
                            color: Color(0xff3C3950),
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Center(
                                    child: Text("No",
                                        style: style.copyWith(
                                            color: Colors.white,
                                            fontSize: 18,
                                            fontFamily: 'Source Sans Pro'))),
                              ],
                            ))),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context, true);
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
                        height: 50,
                        width: 55,
                        child: Card(
                            color: Color(0xff3C3950),
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Center(
                                    child: Text("Yes",
                                        style: style.copyWith(
                                            color: Colors.white,
                                            fontSize: 18,
                                            fontFamily: 'Source Sans Pro'))),
                              ],
                            ))),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  static showDialogContent(BuildContext context, String msg) {
    // return object of type Dialog
    var prefs = SharedPrefs();

    return AlertDialog(
        title: new Center(
          child: Text(
            "Alert!!",
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Source Sans Pro',
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        content: new Text(
          msg,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Source Sans Pro',
            fontSize: 16.0,
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text(
              "Ok",
              style: TextStyle(
                  color: Colors.red, fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.of(context).pop(true);
              prefs.clearPrefs();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => Login()),
                  (Route<dynamic> route) => false);
            },
          ),
        ]);
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
