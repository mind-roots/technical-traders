import 'package:technicaltraders/details/itemModel.dart';
import 'package:technicaltraders/home/homeModel.dart';
import 'package:technicaltraders/home/homeModelLogout.dart';
import 'package:technicaltraders/home/topics_model.dart';
import 'package:technicaltraders/latestPost/latestPostDetail.dart';
import 'package:technicaltraders/latestPost/latestPostModel.dart';
import 'package:technicaltraders/login/LoginModel.dart';
import 'package:technicaltraders/notification/notificationModel.dart';
import 'package:technicaltraders/notification/notificationSwithModel.dart';
import 'package:technicaltraders/portfolio/portfolioPostModel.dart';

import 'api_provider.dart';

class Repository {
  final apiProvider = ApiProvider();

  Future<ModelLogin> fetchLoginData(
          userName, password, device_type, device_id) =>
      apiProvider.fetchLoginData(userName, password, device_type, device_id);

  Future<ModelLogin> fetchLoginHomeData(
          auth, userName, password, device_type, device_id) =>
      apiProvider.fetchHomeLoginData(
          auth, userName, password, device_type, device_id);

  Future<HomeModel> fetchHomeData(auth_code) =>
      apiProvider.fetchHomeData(auth_code);

  Future<HomeModelLogout> fetchAuthCode(
    auth_code,
    device_id,
    device_type,
  ) =>
      apiProvider.authCode(auth_code, device_id, device_type);

  Future<TopicsModel> fetchTopics(device_id) =>
      apiProvider.fetchTopics(device_id);

  Future<HomeModelLogout> logout(auth_code) => apiProvider.logout(auth_code);

  Future<ItemModel> fetchItemData(
          auth_code, resources_link, int perPage, String offset) =>
      apiProvider.fetchItemData(
          auth_code, resources_link, perPage.toString(), offset);

  Future<LatestPostModel> getPostdetail(authCode, resourcesLink, postId) =>
      apiProvider.getPostdetail(authCode, resourcesLink, postId.toString());

  Future<PortfolioModel> getPortfolio(
          authCode, resourcesLink, int index, education) =>
      apiProvider.getPortfolio(authCode, resourcesLink, index, education);

  Future<NotificationModel> viewNotification(authCode) =>
      apiProvider.viewNotification(authCode);

  Future<NotificationSwitchModel> notificationControl(
          authCode, notificationSwitch) =>
      apiProvider.notificationControl(authCode, notificationSwitch);

  Future<NotificationSwitchModel> checkNotification(authCode) =>
      apiProvider.checkNotification(authCode);
}
