import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  final String _auth = "auth_code";
  final String _user = "username";
  final String _pass = "password";
  final String _LoggedIn = "logged_in";
  final String _ClearCart = "clear_cart";
  final String SavePosition = "save_position";
  final String Collection_id = "collection_id";
  final String _notificationStatus = "notification_status";

  /// ------------------------------------------------------------
  /// Method that returns the user authCode to hit web service
  /// ------------------------------------------------------------
  Future<String> getAuthCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_auth) ?? "";
  }

  Future<String> getNotificationStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_notificationStatus) ?? "";
  }

  Future<String> getUserName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_user) ?? "";
  }
  Future<String> getPassword() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_pass) ?? "";
  }

  Future<bool> login() async {
    // Simulate a future for response after 2 second.
    String auth = await getAuthCode();
    if (auth.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<bool> notificationStatus() async {
    // Simulate a future for response after 2 second.
    String status = await getNotificationStatus();
    if (status.isNotEmpty) {
      if (status == "on") {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /// ------------------------------------------------------------
  /// Method that save the user Auth
  /// ------------------------------------------------------------
  Future<bool> setAuthCode(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_auth, value);
  }

  Future<bool> setUserPass(String user,String pass) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_user, user);
    prefs.setString(_pass, pass);
    return true;
  }

  Future<bool> setNotificationStatus(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_notificationStatus, value);
  }

  /// ------------------------------------------------------------
  /// Method that save the user Auth
  /// ------------------------------------------------------------
  Future<bool> setLoggedInstance(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_LoggedIn, value);
  }

  /// ------------------------------------------------------------
  Future<bool> clearCartInstance(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_ClearCart, value);
  }

  /// ------------------------------------------------------------
  /// Method that returns the user authCode to hit web service
  /// ------------------------------------------------------------
  Future<String> alreadyClearedInstance() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_ClearCart) ?? "no";
  }

  /// ------------------------------------------------------------
  /// Method that returns the user authCode to hit web service
  /// ------------------------------------------------------------
  Future<String> getLoggedInstance() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_LoggedIn) ?? "no";
  }

  /// ------------------------------------------------------------
  /// Method that save Collection id on saving product to cart
  /// ------------------------------------------------------------
  Future<bool> setCollectionId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(Collection_id, value);
  }

  /// ------------------------------------------------------------
  /// Method that save Collection id on saving product to cart
  /// ------------------------------------------------------------
  Future<String> getCollectionId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(Collection_id) ?? "";
  }

  /// ------------------------------------------------------------
  Future<bool> savePosition(int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(SavePosition, value) ?? 0;
  }

  /// ------------------------------------------------------------
  Future<int> getPosition() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(SavePosition) ?? 0;
  }

  Future<bool> clearPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.remove(_auth);
    await prefs.clear();
    return true;
  }
}
