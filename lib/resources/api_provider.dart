import 'dart:convert';
import 'dart:developer' as developer;
import 'package:http/http.dart';
import 'package:technicaltraders/details/itemModel.dart';
import 'package:technicaltraders/home/homeModel.dart';
import 'package:technicaltraders/home/homeModelLogout.dart';
import 'package:technicaltraders/home/topics_model.dart';
import 'package:technicaltraders/latestPost/latestPostModel.dart';
import 'package:technicaltraders/login/LoginModel.dart';
import 'package:technicaltraders/notification/notificationModel.dart';
import 'package:technicaltraders/notification/notificationSwithModel.dart';
import 'package:technicaltraders/portfolio/portfolioPostModel.dart';

class ApiProvider {
  Client client = Client();
  final String baseUrl =
//      "http://cloudart.com.au/projects/multi_site_blog/index.php/api/data_v1";
      "https://www.thetechnicaltraders.com/appapi/index.php/api/data_v1";

  Future<ModelLogin> fetchLoginData(String userName, String password,
      String device_type, String device_id) async {
    Map map = {
      'username': userName,
      'password': password,
      'device_type': device_type,
      'device_id': device_id
    };
    final response = await client.post("$baseUrl/login", body: map);
    print("login" + response.body.toString());
    if (response.statusCode == 200) {
      return ModelLogin.fromJson(json.decode(response.body));
    } else {
//      throw Exception('Technical Traders: Failed to load Login service');
      return ModelLogin.fromJson({
        "status": "false",
        "msg": response.body.toString(),
      });
    }
  }

  Future<ModelLogin> fetchHomeLoginData(String auth, String userName,
      String password, String device_type, String device_id) async {
    Map map = {
      'auth_code': auth,
      'username': userName,
      'password': password,
      'device_type': device_type,
      'device_id': device_id
    };
    final response = await client.post("$baseUrl/loginauth", body: map);
    print("loginHome" + response.body.toString());
    if (response.statusCode == 200) {
      return ModelLogin.fromJson(json.decode(response.body));
    } else {
//      throw Exception('Technical Traders: Failed to load Login service');
      return ModelLogin.fromJson({"status": "error"});
    }
  }

  Future<HomeModel> fetchHomeData(String auth_code) async {
    Map map = {'auth_code': auth_code};
    // final response = await client.post("$baseUrl/getResources", body: map);
    final response = await client.post("$baseUrl/getResourcesV2", body: map);
    developer.log("getResources : " + response.body.toString());
    if (response.statusCode == 200) {
      return HomeModel.fromJson(json.decode(response.body));
    } else {
      return HomeModel.fromJson({"status": "error"});
    }
  }

  Future<ItemModel> fetchItemData(String authCode, String resourcesLink,
      String perPage, String offset) async {
    // print("resources_link:   " + resources_link);
//    Map map = {
//      'auth_code': authCode,
//      'resources_link': resourcesLink,
//      'per_page': offset,
//      'offset': perPage.toString()
//    };
    Map<String, String> map = {
      'Access-Control-Request-Headers': "X-WP-Total",
    };

    final response = await client.get(
        resourcesLink + "wp-json/wp/v2/posts?per_page=$offset&offset=$perPage",
        headers: map);
    print("getPosts" + response.body.toString());
    if (response.statusCode == 200) {
      String abc = response.headers["x-wp-total"].toString();
      return ItemModel.fromJson(json.decode(response.body), abc);
    } else {
      return ItemModel.fromJson([
        {"status": "error"}
      ], "0");
//      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<HomeModelLogout> authCode(
    String authCode,
    String deviceId,
    String deviceType,
  ) async {
    // print("resources_link:   " + resources_link);
    Map map = {
      'auth_code': authCode,
      'device_id': deviceId,
      'device_type': deviceType
    };
    final response = await client.post("$baseUrl/authCode", body: map);
    print("authCode" + response.body.toString());
    if (response.statusCode == 200) {
      return HomeModelLogout.fromJson(json.decode(response.body));
    } else {
      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<TopicsModel> fetchTopics(String deviceId) async {
    // print("resources_link:   " + resources_link);
    Map<String, String> map = {
      'Authorization':
          "key=AAAA7Z2m8CM:APA91bHnPcCCkiH0nsvy-T7aEtwpE1GHsWxMQbLfLfOpPdrD5XvziGGyDPYtNw9ZYWLjxtlBr2JKdPBfQ5IIQ0irYjK0mVMR7sxVeFSWt2rTyvgciJM-jRGCaWb3IrqDVia4fAmh_SSa",
      'Content-Type': 'application/json'
    };
    final response = await client.post(
        "https://iid.googleapis.com/iid/info/$deviceId?details=true",
        headers: map);
    print("authCode" + response.body.toString());
    if (response.statusCode == 200) {
      return TopicsModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<HomeModelLogout> logout(
    String auth_code,
  ) async {
    // print("resources_link:   " + resources_link);
    Map map = {
      'auth_code': auth_code,
    };
    final response = await client.post("$baseUrl/logout", body: map);
    print("logout" + response.body.toString());
    if (response.statusCode == 200) {
      return HomeModelLogout.fromJson(json.decode(response.body));
    } else {
      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<LatestPostModel> getPostdetail(
      String auth_code, String resources_link, String post_id) async {
    // print("resources_link:   " + resources_link);
    Map map = {
      'auth_code': auth_code,
      'resources_link': resources_link,
      'post_id': post_id,
    };
    final response = await client.post("$baseUrl/getPostdetail", body: map);
    print("getPostdetail" + response.body.toString());
    if (response.statusCode == 200) {
      return LatestPostModel.fromJson(json.decode(response.body));
    } else {
      return LatestPostModel.fromJson({"status": "error"});
//      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<PortfolioModel> getPortfolio(
      String auth_code, String resources_link, int index, education) async {
    // print("resources_link:   " + resources_link);
    print("link: $resources_link");
    print("education: $education");
    print("index: $index");
    Map map = index == 1
        ? {
            'auth_code': auth_code,
            'resources_link': resources_link,
          }
        : index == 3
            ? {
                'auth_code': auth_code,
                'resources_link': resources_link,
                'hotlink': "hotlink",
              }
            : {
                'auth_code': auth_code,
                'resources_link': resources_link,
                'education': education,
              };

    var endPoint = index == 1
        ? "getportfolio"
        : index == 3
            ? "getBanHotlink"
            : "getEducation";
    var resultPoint = "$baseUrl/$endPoint";
    print("Params: $map");
    print("endPoint: $resultPoint");
    final response = await client.post(resultPoint, body: map);
    print("getportfolio:  " + response.body.toString());
    if (response.statusCode == 200) {
      return PortfolioModel.fromJson(json.decode(response.body));
    } else {
      return PortfolioModel.fromJson({"status": "error"});
//      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<NotificationModel> viewNotification(String authCode) async {
    // print("resources_link:   " + resources_link);
    Map map = {
      'auth_code': authCode,
    };
    final response = await client.post("$baseUrl/viewNotification", body: map);
    print("viewNotification" + response.body.toString());
    if (response.statusCode == 200) {
      return NotificationModel.fromJson(json.decode(response.body));
    } else {
      return NotificationModel.fromJson({"status": "error"});
//      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<NotificationSwitchModel> notificationControl(
      String authCode, String notificationSwitch) async {
    // print("resources_link:   " + resources_link);
    Map map = {'auth_code': authCode, 'notification': notificationSwitch};
    final response =
        await client.post("$baseUrl/notificationControll", body: map);
    print("notificationControll" + response.body.toString());
    if (response.statusCode == 200) {
      return NotificationSwitchModel.fromJson(json.decode(response.body));
    } else {
      return NotificationSwitchModel.fromJson({"status": "error"});
//      throw Exception('Technical Traders: Failed to load Login service');
    }
  }

  Future<NotificationSwitchModel> checkNotification(String authCode) async {
    // print("resources_link:   " + resources_link);
    Map map = {'auth_code': authCode};
    final response = await client.post("$baseUrl/checknotification", body: map);
    print("checknotification" + response.body.toString());
    if (response.statusCode == 200) {
      return NotificationSwitchModel.fromJson(json.decode(response.body));
    } else {
      return NotificationSwitchModel.fromJson({"status": "error"});
//      throw Exception('Technical Traders: Failed to load Login service');
    }
  }
}
