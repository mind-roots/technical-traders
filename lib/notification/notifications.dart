import 'package:custom_switch/custom_switch.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:technicaltraders/latestPost/latestPostDetail.dart';
import 'package:technicaltraders/login/Login.dart';
import 'package:technicaltraders/notification/notificationBloc.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'notificationModel.dart';

class Notifications extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => NotificationState();
}

class NotificationState extends State<Notifications> {
  var bloc = NotificationBloc();
  NotificationModel itemsData;
  SharedPrefs notificationCheck = new SharedPrefs();
  bool _load = false;
  bool _changed = false;
  static bool switchControl = false;
  var textHolder = 'Switch is OFF';
  DateFormat dateFormat = DateFormat("yyyy-MMM-dd  HH:mm:ss");
  DateFormat dateFormatParse = DateFormat("yyyy-MM-dd HH:mm:ss");
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var prefs = SharedPrefs();

  @override
  void initState() {
    super.initState();
    bloc.viewNotification();
    checkStatus();
    setState(() {
      _load = true;
    });

    bloc.itemData.listen((response) {
      if (response.status == "true") {
        setState(() {
          itemsData = response;
          _load = false;
        });
      } else if (response.status == "false") {
        setState(() {
          _load = false;
        });
        if (response.msg.isNotEmpty) {
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return WillPopScope(
                onWillPop: () {},
                child: new AlertDialog(
                  title: new Center(
                    child: Text(
                      "Alert!!",
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Source Sans Pro',
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  content: new Text(
                    response.msg,
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 18.0,
                    ),
                  ),
                  actions: <Widget>[
                    // usually buttons at the bottom of the dialog
                    new FlatButton(
                      child: new Text(
                        "Ok",
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(true);
                        prefs.clearPrefs();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (context) => Login()),
                            (Route<dynamic> route) => false);
                      },
                    ),
                  ],
                ),
              );
            },
          );
        }
      } else {
        setState(() {
          _load = false;
        });
      }
    });

    bloc.itemDataSwitch.listen((response) async {
      if (response.status == "true") {
        switchControl = await notificationCheck.notificationStatus();
        setState(() {
          switchControl;
          _load = false;
        });
      } else {
        setState(() {
          _load = false;
        });
      }
    });
  }

  void checkStatus() async {
    switchControl = (await notificationCheck.notificationStatus());
    setState(() {
      switchControl;
    });
  }

  void toggleSwitch(bool value) {
    if (switchControl == false) {
      setState(() {
        _load = true;
      });
      bloc.notificationControl("1");
      // Put your code here which you want to execute on Switch ON event.

    } else {
      setState(() {
        _load = true;
      });
      bloc.notificationControl("0");
      // Put your code here which you want to execute on Switch OFF event.
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            color: Colors.white,
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios,
              color: Color(0xff3C3950), size: 20.0),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        title: Image.asset('assets/images/logo.png',
            height: 50.0, fit: BoxFit.cover),
        actions: <Widget>[
          IconButton(
            icon:
                new Icon(Icons.settings, color: Color(0xff3C3950), size: 20.0),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog("Settings"),
            ).then((onValue) => {
                  switchControl
                      ? bloc.notificationControl("1")
                      : bloc.notificationControl("0")
                }),
          ),
//                child:
//                Switch(
//                  onChanged: toggleSwitch,
//                  value: switchControl,
//                  activeColor: Color(0xff3C3950),
//                  activeTrackColor: Colors.grey,
//                  inactiveThumbColor: Colors.white,
//                  inactiveTrackColor: Colors.grey,
//                ),
//            ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(0.0),
            color: Color(0xff3C3950),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(64.0),
                  bottomRight: Radius.circular(64.0)),
              shape: BoxShape.rectangle,
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(16.0, 10.0, 0.0, 0.0),
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Updates & Signal Notifications",
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Source Sans Pro',
                            color: Color(0xff3C3950),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Flexible(
                  child: itemsData != null
                      ? Container(
                          margin: EdgeInsets.fromLTRB(0.0, 12.0, 4.0, 44.0),
                          child: ListView.builder(
                              itemCount: itemsData.resource.length,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  margin:
                                      EdgeInsets.fromLTRB(16.0, 8.0, 4.0, 8.0),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 0.3, color: Color(0xff949494)),
                                    ),
                                    color: Colors.white,
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          new Flexible(
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  16.0, 8.0, 4.0, 8.0),
                                              color: Color(0xff3C3950),
                                              width: 6.0,
                                              height: 90.0,
                                            ),
                                            flex: 1,
                                          ),
                                          Flexible(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: ListTile(
                                                isThreeLine: true,
                                                title: Align(
                                                  child: Text(
                                                    itemsData.resource[index]
                                                        .postTitle,
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xff3C3950),
                                                        fontSize: 18.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily:
                                                            'Source Sans Pro'),
                                                  ),
                                                  alignment:
                                                      Alignment.centerLeft,
                                                ),
                                                subtitle: Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  margin:
                                                      EdgeInsets.only(top: 6),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text(
                                                          dateFormat.format(
                                                              dateFormatParse
                                                                  .parse(itemsData
                                                                      .resource[
                                                                          index]
                                                                      .postDate)),
                                                          style: TextStyle(
                                                              color: Color(
                                                                  0xff949494),
                                                              fontSize: 18.0,
                                                              fontFamily:
                                                                  'Source Sans Pro')),
                                                    ],
                                                  ),
                                                ),
                                                onTap: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          LatestPostDetail(
                                                              int.parse(
                                                                  itemsData
                                                                      .resource[
                                                                          index]
                                                                      .postId),
                                                              itemsData
                                                                  .resource[
                                                                      index]
                                                                  .resourcesLink),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                            flex: 10,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        )
                      : Container(),
                  flex: 9,
                )
              ],
            ),
          ),
          new Align(
            child: loadingIndicator,
            alignment: FractionalOffset.center,
          ),
        ],
      ),
    );
  }
}

class CustomDialog extends StatefulWidget {
  State<StatefulWidget> createState() => CustomDialogState();
  var disputeNoteController = TextEditingController();
  var title;

  CustomDialog(String s) {
    this.title = s;
  }
}

class CustomDialogState extends State<CustomDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 16,
            right: 16,
          ),
          margin: EdgeInsets.only(top: 2),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
//                        child: Text(
//                          "Alert",
//                          style: style.copyWith(
//                              color: Colors.black,
//                              fontSize: 22,
//                              fontFamily: 'Source Sans Pro'),
//                          textAlign: TextAlign.left,
//                        ),
                      ),
                      SizedBox(height: 8.0),
                      Container(
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: Text(
                          widget.title,
                          style: style.copyWith(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: 'Source Sans Pro',
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 10.0),
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    "Notification",
                                    style: style.copyWith(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontFamily: 'Source Sans Pro'),
                                  ),
                                  flex: 1,
                                ),
                                CustomSwitch(
                                  onChanged: toggleSwitch,
                                  value: NotificationState.switchControl,
                                  activeColor: Color(0xff3C3950),
                                ),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context, true);
                                  },
                                  child: Container(
                                      margin: EdgeInsets.fromLTRB(0, 14, 0, 0),
                                      height: 40,
                                      width: 75,
                                      child: Card(
                                          color: Color(0xff3C3950),
                                          elevation: 4,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          child: Stack(
                                            children: <Widget>[
                                              Center(
                                                  child: Text("Close",
                                                      style: style.copyWith(
                                                          color: Colors.white,
                                                          fontSize: 18,
                                                          fontFamily:
                                                              'Source Sans Pro'))),
                                            ],
                                          ))),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 8.0),
                    ],
                  )),
            ],
          ),
        ),
      ],
    );
  }

  void toggleSwitch(bool value) {
    if (NotificationState.switchControl == false) {
      setState(() {
        NotificationState.switchControl = true;
//        textHolder = 'Switch is ON';
      });
      print('Switch is ON');
      // Put your code here which you want to execute on Switch ON event.

    } else {
      setState(() {
        NotificationState.switchControl = false;
//        textHolder = 'Switch is OFF';
      });
      print('Switch is OFF');
      // Put your code here which you want to execute on Switch OFF event.
    }
  }
}
