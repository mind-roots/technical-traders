import 'package:technicaltraders/notification/notificationDetailsModel.dart';

class NotificationModel {
  String status = "";
  String msg = "";
  List<NotificationDetailsModel> resource;

  NotificationModel.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];

    if (status == "true") {
      msg = parsedJson['msg'];
      List<NotificationDetailsModel> temp = [];
      for (int i = 0; i < parsedJson['notification_data'].length; i++) {
        NotificationDetailsModel result =
            NotificationDetailsModel(parsedJson['notification_data'][i]);
        temp.add(result);
      }
      temp.sort((a, b) {
        var adate = a.postDate; //before -> var adate = a.expiry;
        var bdate = b.postDate; //before -> var bdate = b.expiry;
        return adate.compareTo(
            bdate); //to get the order other way just switch `adate & bdate`
      });
      resource = temp.reversed.toList();
    } else if (status == "false") {
      msg = parsedJson['msg'];
    }
  }
}
