import 'package:rxdart/rxdart.dart';
import 'package:technicaltraders/notification/notificationModel.dart';
import 'package:technicaltraders/notification/notificationSwithModel.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:technicaltraders/resources/repository.dart';

class NotificationBloc {
  final _repository = Repository();
  final _notificationFetcher = PublishSubject<NotificationModel>();
  final _notificationSwitchFetcher = PublishSubject<NotificationSwitchModel>();
  var prefs = SharedPrefs();

  Observable<NotificationModel> get itemData => _notificationFetcher.stream;

  Observable<NotificationSwitchModel> get itemDataSwitch => _notificationSwitchFetcher.stream;

  viewNotification() async {
    var authCode = await prefs.getAuthCode();
    NotificationModel itemModel = await _repository.viewNotification(authCode);
    _notificationFetcher.sink.add(itemModel);
  }

  notificationControl(String notificationSwitch) async {
    var authCode = await prefs.getAuthCode();
    NotificationSwitchModel itemModel =
        await _repository.notificationControl(authCode, notificationSwitch);
    _notificationSwitchFetcher.sink.add(itemModel);
    if (itemModel.status == "true") {
      prefs.setNotificationStatus(itemModel.msg);
    }
  }

  dispose() {
    _notificationFetcher.close();
    _notificationSwitchFetcher.close();
  }
}
