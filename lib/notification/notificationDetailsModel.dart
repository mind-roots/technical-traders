


class NotificationDetailsModel {
  String id;
  String postId;
  String postTitle;
  String postDate;
  String resourcesLink;
  String createdAt;

  NotificationDetailsModel(collection) {
    id = collection["id"];
    postId = collection["post_id"];
    postTitle = collection["post_title"];
    postDate = collection["post_date"];
    resourcesLink = collection["resources_link"];
    createdAt = collection["created_at"];
  }
}
