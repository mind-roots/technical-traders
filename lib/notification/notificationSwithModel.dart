import 'package:technicaltraders/notification/notificationDetailsModel.dart';

class NotificationSwitchModel {
  String status = "";
  String msg = "";

  NotificationSwitchModel.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];

    if (status == "true") {
      msg = parsedJson['msg'];
    } else if (status == "false") {
      msg = parsedJson['msg'];
    }
  }
}
