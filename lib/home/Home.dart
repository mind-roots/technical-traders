import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:technicaltraders/Constants.dart';
import 'package:technicaltraders/details/newsletter.dart';
import 'package:technicaltraders/home/HomeBloc.dart';
import 'package:technicaltraders/login/LoginBloc.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';

import '../login/Login.dart';
import '../notification/notifications.dart';
import 'homeModel.dart';
import 'homeModelLogout.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextStyle style = TextStyle(fontFamily: 'Source Sans Pro', fontSize: 20.0);

  var bloc = HomeBloc();
  HomeModel homeModel;
  HomeModelLogout homeModelLogout;
  bool _load = false;
  var prefs = SharedPrefs();
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool isOnline = false;
  Connectivity connectivity = Connectivity();
  var blocLogin = LoginBloc();
  List<String> checkTopics = [];
  String firebaseToken = "";

  @override
  void initState() {
    super.initState();
    initialise();
    bloc.itemTopicCheck.listen((response) {
      if (response.topics != null) {
        if (response.wbn == "0" && checkTopics.contains("wbn")) {
          fcmSubscribe("wbn");
        } else if (response.wbn == "1" && !checkTopics.contains("wbn")) {
          fcmUnSubscribe("wbn");
        }
        if (response.tmtf == "0" && checkTopics.contains("tmtf")) {
          fcmSubscribe("tmtf");
        } else if (response.tmtf == "1" && !checkTopics.contains("tmtf")) {
          fcmUnSubscribe("tmtf");
        }
        if (response.twa == "0" && checkTopics.contains("twa")) {
          fcmSubscribe("twa");
        } else if (response.twa == "1" && !checkTopics.contains("twa")) {
          fcmUnSubscribe("twa");
        }
        if (response.ots == "0" && checkTopics.contains("ots")) {
          fcmSubscribe("ots");
        } else if (response.ots == "1" && !checkTopics.contains("ots")) {
          fcmUnSubscribe("ots");
        }
      }
    });
    blocLogin.loginData1.listen((response) {
      if (response.status == "true") {
        if (response.profile.resource != null) {
          for (int i = 0; i < response.profile.resource.length; i++) {
            if (response.profile.resource[i].title ==
                "The Technical Investor") {
              checkTopics.add("tmtf");
//              fcmSubscribe("tmtf");
            }
            if (response.profile.resource[i].title == "BAN Trader Pro") {
              checkTopics.add("wbn");
//              fcmSubscribe("wbn");
            }
            if (response.profile.resource[i].title ==
                "Technical Index & Bond Trader") {
              checkTopics.add("twa");
//              fcmSubscribe("twa");
            }
            if (response.profile.resource[i].title ==
                "Options Trading Signals") {
              checkTopics.add("ots");
//              fcmSubscribe("twa");
            }
          }
        }
        bloc.fetchHomeData();
        bloc.notificationControl();
        bloc.fetchTopics(firebaseToken);
        return;
      } else {
        setState(() {
          _load = false;
        });
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return WillPopScope(
              onWillPop: () {},
              child: new AlertDialog(
                title: new Center(
                  child: Text(
                    "Alert!!",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Source Sans Pro',
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                content: new Text(
                  response.msg,
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Source Sans Pro',
                    fontSize: 18.0,
                  ),
                ),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text(
                      "Ok",
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                      prefs.clearPrefs();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => Login()),
                          (Route<dynamic> route) => false);
                    },
                  ),
                ],
              ),
            );
          },
        );
      }
    });
    bloc.homeDataLogout.listen((response) {
      if (response.status == "true") {
        try {
          if (checkTopics.contains("wbn")) {
            fcmUnSubscribe("wbn");
          }
          if (checkTopics.contains("tmtf")) {
            fcmUnSubscribe("tmtf");
          }
          if (checkTopics.contains("twa")) {
            fcmUnSubscribe("twa");
          }
        } catch (Exception) {}
        prefs.clearPrefs();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false);
        homeModelLogout = response;
      }
    });
    bloc.homeData.listen((response) {
      if (response.status == "true") {
        setState(() {
          homeModel = response;
          _load = false;
        });
      } else if (response.msg.isNotEmpty) {
        setState(() {
          _load = false;
        });

        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Center(
                child: Text(
                  "Alert!!",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              content: new Text(
                response.msg,
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 18.0,
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(
                    "Ok",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                    try {
                      if (checkTopics.contains("wbn")) {
                        fcmUnSubscribe("wbn");
                      }
                      if (checkTopics.contains("tmtf")) {
                        fcmUnSubscribe("tmtf");
                      }
                      if (checkTopics.contains("twa")) {
                        fcmUnSubscribe("twa");
                      }
                    } catch (Exception) {}
                    prefs.clearPrefs();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => Login()),
                        (Route<dynamic> route) => false);
                  },
                ),
              ],
            );
          },
        );
      } else {
        setState(() {
          _load = false;
        });
        showDialog(
          context: context,
          builder: (BuildContext context) =>
              CustomDialog(bloc, "Failure, Do you want to reload data?", 1),
        ).then((res) {
          if (res != null && res) {
            //Navigator.of(context).pop();
          }
        });
      }
    });
    //firebase message
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        // var jj = json.decode(message.toString());
        var body = message['notification']['body'];
        var title = message['notification']['title'];
        var body1 = body;
        if (body == null || body == "null") {
          body = "A new message received";
        }
        if (title == null || title == "null") {
          title = "Alert!";
        }
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Center(
                child: Text(
                  title,
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              content: new Text(
                body,
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 18.0,
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(
                    "Ok",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Source Sans Pro'),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Notifications()),
                    );
                  },
                ),
              ],
            );
          },
        );
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResumeHome");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Notifications()),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunchHome");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Notifications()),
        );
      },
    );
  }

  void iosPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void initialise() async {
    ConnectivityResult result = await connectivity.checkConnectivity();
    if (result != ConnectivityResult.none) {
      bool resultSecond = await DataConnectionChecker().hasConnection;
      if (resultSecond) {
        isOnline = true;
        if (Platform.isIOS) {
          blocLogin.fetchHomeLoginData(
              await prefs.getUserName(), await prefs.getPassword(), "2", "");
        } else {
          blocLogin.fetchHomeLoginData(
              await prefs.getUserName(), await prefs.getPassword(), "1", "");
          print(await prefs.getPassword());
          print(await prefs.getUserName());
        }
        setState(() {
          _load = true;
        });
        if (Platform.isIOS) iosPermission();
        _firebaseMessaging.getToken().then((token) {
          firebaseToken = token;
          print("TTTTOOOKKEEENNNZZZZZZZZZZZZ : " + token);
          if (Platform.isIOS) {
            bloc.fetchAuthCode("2", token);
          } else if (Platform.isAndroid) {
            bloc.fetchAuthCode("1", token);
          }
        });
      } else {
        Constants.showToast(context, "Please enable internet");
        isOnline = false;
      }
    } else {
      Constants.showToast(context, "Please enable internet");
      isOnline = false;
    }
    connectivity.onConnectivityChanged.listen((result) async {
      if (result != ConnectivityResult.none) {
        bool resultSecond = await DataConnectionChecker().hasConnection;
        if (resultSecond) {
          isOnline = true;
          if (Platform.isIOS) {
            blocLogin.fetchHomeLoginData(
                await prefs.getUserName(), await prefs.getPassword(), "2", "");
          } else {
            blocLogin.fetchHomeLoginData(
                await prefs.getUserName(), await prefs.getPassword(), "1", "");
            print(await prefs.getPassword());
            print(await prefs.getUserName());
          }
          setState(() {
            _load = true;
          });
          if (Platform.isIOS) iosPermission();
          _firebaseMessaging.getToken().then((token) {
            print("TTTTOOOKKEEENNNZZZZZZZZZZZZ : " + token);
            if (Platform.isIOS) {
              bloc.fetchAuthCode("2", token);
            } else if (Platform.isAndroid) {
              bloc.fetchAuthCode("1", token);
            }
          });
        }
      } else {
        Constants.showToast(context, "Please enable internet");
        isOnline = false;
      }
    });
  }

  void fcmSubscribe(String topic) {
    _firebaseMessaging.subscribeToTopic(topic);
  }

  void fcmUnSubscribe(String topic) {
    _firebaseMessaging.unsubscribeFromTopic(topic);
  }

  void _checkStatus(ConnectivityResult result) async {
    isOnline = false;
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        isOnline = true;
        if (Platform.isIOS) {
          blocLogin.fetchHomeLoginData(
              await prefs.getUserName(), await prefs.getPassword(), "2", "");
        } else {
          blocLogin.fetchHomeLoginData(
              await prefs.getUserName(), await prefs.getPassword(), "1", "");
          print(await prefs.getPassword());
          print(await prefs.getUserName());
        }
        setState(() {
          _load = true;
        });
        if (Platform.isIOS) iosPermission();
        _firebaseMessaging.getToken().then((token) {
          print("TTTTOOOKKEEENNNZZZZZZZZZZZZ : " + token);
          if (Platform.isIOS) {
            bloc.fetchAuthCode("2", token);
          } else if (Platform.isAndroid) {
            bloc.fetchAuthCode("1", token);
          }
        });
      } else {
        Constants.showToast(context, "Please enable internet");
        isOnline = false;
      }
    } on SocketException catch (_) {
      Constants.showToast(context, "Please enable internet");
      isOnline = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            color: Colors.white,
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: Image.asset('assets/images/logout.png'),
            onPressed: () async {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    CustomDialog(bloc, "Are you sure you want to logout?", 2),
              ).then((res) {
                if (res != null && res) {
                  //Navigator.of(context).pop();
                }
              });
            },
          );
        }),
        backgroundColor: Color(0xffFEFEFE),
        title: Image.asset('assets/images/logo.png',
            height: 50.0, fit: BoxFit.cover),
        actions: <Widget>[
          IconButton(
            icon: Image.asset('assets/images/notification.png'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Notifications()),
              );
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(0.0),
            color: Color(0xff3C3950),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(64.0),
                  bottomRight: Radius.circular(64.0)),
              shape: BoxShape.rectangle,
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        alignment: Alignment.topCenter,
                        margin: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                        child: Text(
                          "Trading and Investing Alert Services",
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Source Sans Pro'),
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                ),
                Flexible(
                  child: homeModel != null
                      ? Container(
                          margin: EdgeInsets.fromLTRB(6.0, 12.0, 6.0, 28.0),
                          child: ListView.builder(
                            itemCount: homeModel.resource.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => NewsLetter(
                                            homeModel.resource[index].link,
                                            homeModel.resource[index].title,
                                            homeModel.resource[index].image,
                                            homeModel.resource[index].portfolio,
                                            homeModel.resource[index].color,
                                            homeModel
                                                .resource[index].education),
                                      ),
                                    ).then((value) => {
                                          _firebaseMessaging.configure(
                                            onMessage: (Map<String, dynamic>
                                                message) async {
                                              //print('on message $message');
                                              // var jj = json.decode(message.toString());
                                              var body = message['notification']
                                                  ['body'];
                                              var title =
                                                  message['notification']
                                                      ['title'];
                                              var body1 = body;
                                              if (title == null ||
                                                  title == "null") {
                                                title = "Alert!";
                                              }
                                              showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  // return object of type Dialog
                                                  return AlertDialog(
                                                    title: new Center(
                                                      child: Text(
                                                        title,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontFamily:
                                                                'Source Sans Pro',
                                                            fontSize: 20.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                    content: new Text(
                                                      body,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily:
                                                            'Source Sans Pro',
                                                        fontSize: 18.0,
                                                      ),
                                                    ),
                                                    actions: <Widget>[
                                                      // usually buttons at the bottom of the dialog
                                                      new FlatButton(
                                                        child: new Text(
                                                          "Ok",
                                                          style: TextStyle(
                                                              color: Colors.red,
                                                              fontSize: 20,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontFamily:
                                                                  'Source Sans Pro'),
                                                        ),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        Notifications()),
                                                          );
                                                        },
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                            onResume: (Map<String, dynamic>
                                                message) async {
                                              print("onResumeHome");
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Notifications()),
                                              );
                                            },
                                            onLaunch: (Map<String, dynamic>
                                                message) async {
                                              print("onLaunchHome");
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Notifications()),
                                              );
                                            },
                                          )
                                        });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.fromLTRB(
                                        16.0, 4.0, 16.0, 4.0),
                                    padding: EdgeInsets.all(24.0),
                                    decoration: BoxDecoration(
                                      color: HexColor.fromHex(
                                          homeModel.resource[index].color),
                                      border: Border.all(color: Colors.white),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(16.0)),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      // this makes the column height hug its content
                                      children: [
                                        Row(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 6.0),
                                                    ),
                                                    Text(
                                                      homeModel.resource[index]
                                                          .title,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontFamily:
                                                              'Source Sans Pro',
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 20.0),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ]),
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0, 8, 0.0, 0),
                                                child: Text(
                                                    homeModel.resource[index]
                                                        .description,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontFamily:
                                                            'Source Sans Pro',
                                                        fontSize: 16.0)),
                                              ),
                                            ),
                                            Column(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      0, 0, 0.0, 2),
                                                  alignment: Alignment.topRight,
                                                  child: Image.network(
                                                    homeModel
                                                        .resource[index].image,
                                                    fit: BoxFit.contain,
                                                    height: 28.0,
                                                  ),
                                                ),
                                              ],
                                            ), // first row
                                          ],
                                        ), // first row
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      : Container(),
                  flex: 10,
                )
              ],
            ),
          ),
          new Align(
            child: loadingIndicator,
            alignment: FractionalOffset.center,
          ),
        ],
      ),
    );
  }
}

class CustomDialog extends StatelessWidget {
  final disputeNoteController = TextEditingController();
  HomeBloc bloc;
  String title;
  int type;

  CustomDialog(HomeBloc bloc, String s, int i) {
    this.bloc = bloc;
    this.title = s;
    this.type = i;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 16,
            bottom: 16,
            left: 16,
            right: 16,
          ),
          margin: EdgeInsets.only(top: 16),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                  alignment: Alignment.topLeft,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
//                        child: Text(
//                          "Alert",
//                          style: style.copyWith(
//                              color: Colors.black,
//                              fontSize: 22,
//                              fontFamily: 'Source Sans Pro'),
//                          textAlign: TextAlign.left,
//                        ),
                      ),
                      SizedBox(height: 8.0),
                      Container(
                          width: double.infinity,
                          child: Text(title,
                              style: style.copyWith(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: 'Source Sans Pro'))),
                      SizedBox(height: 8.0),
                    ],
                  )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context, true);
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => EndTimePage()),
//                  );
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
                      height: 50,
                      width: 55,
                      child: Card(
                        color: Color(0xff3C3950),
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Stack(
                          children: <Widget>[
                            Center(
                              child: Text(
                                "No",
                                style: style.copyWith(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontFamily: 'Source Sans Pro'),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context, true);
                      if (type == 2) {
                        bloc.logout();
                      } else if (type == 1) {
                        bloc.fetchHomeData();
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
                      height: 50,
                      width: 55,
                      child: Card(
                        color: Color(0xff3C3950),
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Stack(
                          children: <Widget>[
                            Center(
                                child: Text("Yes",
                                    style: style.copyWith(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontFamily: 'Source Sans Pro'))),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
