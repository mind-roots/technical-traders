import 'package:meta/meta.dart';

import 'ResourceModel.dart';

class HomeModelLogout {
  String status = "";
  String msg = "";

  HomeModelLogout.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];
    msg = parsedJson['msg'];
  }
}

//final List<HomeModel> homeModel = <HomeModel>[
//  HomeModel(
//      title: 'The Technical Investor',
//      subtitle: 'Long Term Growth Signals:',
//      subtext: 'Big Trends, Big Profit Investing',
//      image: 'assets/statistics.png', color: 0xff19500C),
//  HomeModel(
//      title: 'Technical Wealth Advisor',
//      subtitle: 'Intermediate Trends Signals:',
//      subtext: 'Long Index, Bonds or in Cash',
//      image: 'assets/meeting.png', color: 0xff11337A),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//];
