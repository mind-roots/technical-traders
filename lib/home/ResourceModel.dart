class ResourceModel {
  String link;
  String title;
  String description = 'Long Term Growth Signals:';
  String image = 'assets/statistics.png';
  //String subtext='Big Trends, Big Profit Investing';
  String color = "";
  String portfolio = "";
  String education = "";

  ResourceModel(collection) {
    link = collection["link"];
    title = collection["title"];
    description = collection["description"];
    image = collection["image"];
//    subtext=collection["subtext"];
    color = collection["color"];
    portfolio = collection["portfolio"];
    education = collection["education"];
  }
}
