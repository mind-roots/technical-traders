import 'ResourceModel.dart';

class HomeModel {
  String status = "";
  String msg = "";
  List<ResourceModel> resource;

  HomeModel.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];

    if (status == "true") {
      msg = parsedJson['msg'];

      List<ResourceModel> temp = [];
      for (int i = 0; i < parsedJson['resources'].length; i++) {
        ResourceModel result = ResourceModel(parsedJson['resources'][i]);
        temp.add(result);
      }
      resource = temp;
    } else if (status == "false") {
      msg = parsedJson['msg'];
    }
  }
}

//final List<HomeModel> homeModel = <HomeModel>[
//  HomeModel(
//      title: 'The Technical Investor',
//      subtitle: 'Long Term Growth Signals:',
//      subtext: 'Big Trends, Big Profit Investing',
//      image: 'assets/statistics.png', color: 0xff19500C),
//  HomeModel(
//      title: 'Technical Wealth Advisor',
//      subtitle: 'Intermediate Trends Signals:',
//      subtext: 'Long Index, Bonds or in Cash',
//      image: 'assets/meeting.png', color: 0xff11337A),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//  HomeModel(
//      title: 'The Technical Trader',
//      subtitle: 'Swing Trading Signals:',
//      subtext: 'Index, Sectors, Bonds & Commodities',
//      image: 'assets/money.png', color: 0xff3C3950),
//];
