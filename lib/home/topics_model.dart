import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:technicaltraders/home/ResourceModel.dart';
import 'package:technicaltraders/login/LoginModel.dart';
import 'package:technicaltraders/login/Profile.dart';

class TopicsModel {
  Map<String, dynamic> rel;
  Map<String, dynamic> topics;
  String wbn = "0";
  String tmtf = "0";
  String twa = "0";
  String ots = "0";

  TopicsModel.fromJson(Map<String, dynamic> parsedJson) {
    rel = parsedJson['rel'];
    topics = rel['topics'];
    if (topics.containsKey("wbn")) {
      wbn = "1";
    } else {
      wbn = "0";
    }
    if (topics.containsKey("tmtf")) {
      tmtf = "1";
    } else {
      tmtf = "0";
    }
    if (topics.containsKey("twa")) {
      twa = "1";
    } else {
      twa = "0";
    }
 if (topics.containsKey("ots")) {
   ots = "1";
    } else {
   ots = "0";
    }
  }
}
