import 'package:rxdart/rxdart.dart';
import 'package:technicaltraders/home/homeModel.dart';
import 'package:technicaltraders/home/homeModelLogout.dart';
import 'package:technicaltraders/home/topics_model.dart';
import 'package:technicaltraders/notification/notificationSwithModel.dart';
import 'package:technicaltraders/resources/SharedPrefs.dart';
import 'package:technicaltraders/resources/repository.dart';

class HomeBloc {
  final _repository = Repository();
  final _homeFetcher = PublishSubject<HomeModel>();
  final _homeFetcherLogout = PublishSubject<HomeModelLogout>();
  final _homeFetcherAuth = PublishSubject<HomeModelLogout>();
  final _homeTopicCheck = PublishSubject<TopicsModel>();
  final _notificationSwitchFetcher = PublishSubject<NotificationSwitchModel>();
  var prefs = SharedPrefs();
  Observable<HomeModel> get homeData => _homeFetcher.stream;
  Observable<HomeModelLogout> get homeDataLogout => _homeFetcherLogout.stream;
  Observable<HomeModelLogout> get homeDataAuth => _homeFetcherAuth.stream;
  Observable<NotificationSwitchModel> get itemDataSwitch => _notificationSwitchFetcher.stream;
  Observable<TopicsModel> get itemTopicCheck => _homeTopicCheck.stream;


  fetchHomeData() async {
    var auth_code = await prefs.getAuthCode();
    HomeModel itemModel = await _repository.fetchHomeData(auth_code);

    _homeFetcher.sink.add(itemModel);
  }

  fetchAuthCode(device_type, device_id) async {
    var auth_code = await prefs.getAuthCode();
    HomeModelLogout itemModel =
        await _repository.fetchAuthCode(auth_code,device_id, device_type);

    _homeFetcherAuth.sink.add(itemModel);
  }

  fetchTopics(device_id) async {
    var auth_code = await prefs.getAuthCode();
    TopicsModel itemModel =
        await _repository.fetchTopics(device_id);

    _homeTopicCheck.sink.add(itemModel);
  }

  logout() async {
    var auth_code = await prefs.getAuthCode();
    HomeModelLogout itemModel = await _repository.logout(auth_code);
    _homeFetcherLogout.sink.add(itemModel);
  }

  notificationControl() async {
    var authCode = await prefs.getAuthCode();
    NotificationSwitchModel itemModel =
    await _repository.checkNotification(authCode);
    _notificationSwitchFetcher.sink.add(itemModel);
    if (itemModel.status == "true") {
      prefs.setNotificationStatus(itemModel.msg);
    }
  }


  dispose() {
    _homeFetcher.close();
    _homeFetcherLogout.close();
    _homeFetcherAuth.close();
    _notificationSwitchFetcher.close();
    _homeTopicCheck.close();
  }
}
